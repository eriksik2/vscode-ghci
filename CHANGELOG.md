# Change Log

All notable changes to "vscode-ghci" will be documented in this file.

The structure of this file is based on [Keep a Changelog](http://keepachangelog.com/).

## 0.0.2
- Change: Smaller icons.
- Change: No longer clear interactive shell every time file is saved.
- Fix: Files with spaces in their absolute path couldn't be opened in the interactive shell.

## 0.0.1
- Initial release